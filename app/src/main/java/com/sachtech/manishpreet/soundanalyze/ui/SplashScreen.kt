package com.sachtech.manishpreet.soundanalyze.ui

import android.os.Bundle
import android.os.Handler
import com.sachtech.manishpreet.soundanalyze.R
import com.sachtech.manishpreet.soundanalyze.basepackage.base.BaseActivity
import com.sachtech.manishpreet.soundanalyze.extensions.openActivity

class SplashScreen : BaseActivity() {

    override fun fragmentContainer(): Int {
        return 0
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        Handler().postDelayed({
            openActivity<HomeActivity>()
            finish()
            /* if (isPermissionsGranted(this, arrayOf(
                     Manifest.permission.READ_CONTACTS,
                     Manifest.permission.READ_EXTERNAL_STORAGE,
                     Manifest.permission.WRITE_EXTERNAL_STORAGE,
                     Manifest.permission.WRITE_CONTACTS,
                     Manifest.permission.SEND_SMS,
                     Manifest.permission.READ_SMS,
                     Manifest.permission.CALL_PHONE,
                     Manifest.permission.RECEIVE_SMS,
                     Manifest.permission.CAMERA))
             ) {

             } else {
                 askForPermissions(this, arrayOf(
                     Manifest.permission.READ_CONTACTS,
                     Manifest.permission.WRITE_CONTACTS,
                     Manifest.permission.SEND_SMS,
                     Manifest.permission.READ_EXTERNAL_STORAGE,
                     Manifest.permission.WRITE_EXTERNAL_STORAGE,
                     Manifest.permission.READ_SMS,
                     Manifest.permission.CALL_PHONE,
                     Manifest.permission.RECEIVE_SMS,
                     Manifest.permission.CAMERA))
             }*/

        }, 3000)
    }

}
