package com.sachtech.manishpreet.soundanalyze.basepackage.base

import android.content.Context
import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.sachtech.researchandcreativity.basepackage.base.ProgressDialogFragment


/**
 * * Created by Gurtek Singh on 2/27/2018.
 * Sachtech Solution
 * gurtek@protonmail.ch
 */
abstract class BaseFragment : Fragment() {


    override fun onAttach(context: Context?) {
        super.onAttach(context)

        if (context is BaseAcitivityListener) {
            baseAcitivityListener = context
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(viewToCreate(), container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    @LayoutRes
    protected abstract fun viewToCreate(): Int

    fun showProgress() {
        ProgressDialogFragment.showProgress(activity!!.supportFragmentManager)
    }

    fun showProgress(s: String) {
        ProgressDialogFragment.showProgress(activity!!.supportFragmentManager, s)
    }

    fun hideProgress() {
        ProgressDialogFragment.hideProgress()
    }


    companion object {

        lateinit var baseAcitivityListener: BaseAcitivityListener

    }

}
