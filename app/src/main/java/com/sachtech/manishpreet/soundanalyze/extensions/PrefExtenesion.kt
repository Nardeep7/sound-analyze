package com.sachtech.manishpreet.soundanalyze.extensions

import com.sachtech.manishpreet.soundanalyze.basepackage.controller.App

fun setPrefrence(key: String, value: Any) {
    val preference = App.application.applicationContext.getSharedPreferences("friend_finder", 0)
    val editor = preference.edit()

    when (value) {
        is String -> editor.putString(key, value as String)
        is Boolean -> editor.putBoolean(key, value as Boolean)
    }
    editor.commit()
}

fun clearPrefrences() {
    val preference = App.application.applicationContext.getSharedPreferences("friend_finder", 0)
    val editor = preference.edit()
    editor.clear()
    editor.commit()
}

inline fun <reified T> getPrefrence(key: String, deafultValue: T): T {
    val preference = App.application.applicationContext.getSharedPreferences("friend_finder", 0)
    return when (T::class) {
        String::class -> preference.getString(key, deafultValue as String) as T
        Boolean::class -> preference.getBoolean(key, deafultValue as Boolean) as T
        else -> {
            " " as T
        }
    }

}

/*  inline fun <reified T> setprefObject(key: String,obj:T)
  {
      setPrefrence(key,Gson().toJson(obj))
  }

   inline fun <reified T> getprefObject(key:String): T {
          return Gson().fromJson(getPrefrence(key, ""), T::class.java)
  }*/