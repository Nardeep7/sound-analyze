package com.sachtech.manishpreet.soundanalyze.basepackage.base

import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v4.app.FragmentManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.TextView
import com.sachtech.manishpreet.soundanalyze.R

/**
 * Created by Sachtech on 1/6/2018.
 */

class ProgressDialogFragment : DialogFragment() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setStyle(DialogFragment.STYLE_NORMAL, R.style.CustomDialog)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        dialog.window!!.requestFeature(Window.FEATURE_NO_TITLE)

        return inflater.inflate(R.layout.progress_dialog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (view.findViewById<View>(R.id.message) as TextView).text = arguments!!.getString("message")
        dialog.setCancelable(false)
        dialog.setCanceledOnTouchOutside(false)
    }

    companion object {
        internal var progressDialogFragment: ProgressDialogFragment? = null

        fun newInstance(title: String): ProgressDialogFragment {

            val args = Bundle()
            args.putString("message", title)
            val fragment = ProgressDialogFragment()
            fragment.arguments = args
            return fragment
        }

        fun showProgress(fragmentManager: FragmentManager, Title: String) {
            progressDialogFragment = ProgressDialogFragment.newInstance(Title)
            progressDialogFragment!!.show(fragmentManager, ProgressDialogFragment::class.java.name)
        }

        fun showProgress(fragmentManager: FragmentManager) {
            progressDialogFragment = ProgressDialogFragment.newInstance("")
            progressDialogFragment!!.show(fragmentManager, ProgressDialogFragment::class.java.name)
        }

        fun hideProgress() {
            if (progressDialogFragment != null)
                progressDialogFragment!!.dismiss()
        }
    }
}
