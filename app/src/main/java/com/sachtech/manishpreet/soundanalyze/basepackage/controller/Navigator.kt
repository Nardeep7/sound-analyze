package com.sachtech.manishpreet.soundanalyze.basepackage.controller

import android.content.Intent
import android.os.Bundle
import android.support.annotation.IdRes
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v7.app.AppCompatActivity
import com.example.sachtech.researchandcreativity.basepackage.controller.Transition
import com.sachtech.manishpreet.soundanalyze.basepackage.base.BaseFragment


/**
 * * Created by Gurtek Singh on 2/27/2018.
 * Sachtech Solution
 * gurtek@protonmail.ch
 */

class Navigator(
    private val activity: AppCompatActivity, @param:IdRes private val container: Int,
    private val addTransition: Transition,
    private val replaceTransition: Transition
) {

    /**
     * @param classToOpen class which you want to open
     * @param extras      put extra data which you want in other activity
     */

    fun openActivity(classToOpen: Class<out AppCompatActivity>, extras: Bundle? = null) {
        val intent = Intent(activity, classToOpen)
        if (extras != null) intent.putExtras(extras)
        activity.startActivity(intent)
    }


    @Throws(IllegalAccessException::class, InstantiationException::class)
    private fun performTransition(
        fragment: Class<out BaseFragment>,
        isaddtoStack: Boolean,
        usertransition: Transition,
        extras: Bundle?
    ) {

        val manager = activity.supportFragmentManager
        val className = fragment.javaClass.simpleName
        var baseFragment: Fragment? = manager.findFragmentByTag(className)

        if (baseFragment == null) {
            val transaction = manager.beginTransaction()
            baseFragment = fragment.newInstance()
            usertransition.performTransiton(transaction, container, baseFragment)
            if (isaddtoStack)
                transaction.addToBackStack(className)

            transaction.commitAllowingStateLoss()
        } else {
            manager.popBackStackImmediate(className, FragmentManager.POP_BACK_STACK_INCLUSIVE)
        }

        if (extras != null) baseFragment!!.arguments = extras

    }

    fun addFragment(fragment: Class<out BaseFragment>, isAddtoStack: Boolean) {
        try {
            performTransition(fragment, isAddtoStack, addTransition, null)
        } catch (e: IllegalAccessException) {
            e.printStackTrace()
        } catch (e: InstantiationException) {
            e.printStackTrace()
        }

    }

    fun addFragment(fragment: Class<out BaseFragment>, isAddtoStack: Boolean, extras: Bundle) {
        try {
            performTransition(fragment, isAddtoStack, addTransition, extras)
        } catch (e: IllegalAccessException) {
            e.printStackTrace()
        } catch (e: InstantiationException) {
            e.printStackTrace()
        }

    }


    fun replaceFragment(fragment: Class<out BaseFragment>, isAddtoStack: Boolean) {
        try {
            performTransition(fragment, isAddtoStack, replaceTransition, null)
        } catch (e: IllegalAccessException) {
            e.printStackTrace()
        } catch (e: InstantiationException) {
            e.printStackTrace()
        }

    }

    fun replaceFragment(fragment: Class<out BaseFragment>, isAddtoStack: Boolean, extras: Bundle) {
        try {
            performTransition(fragment, isAddtoStack, replaceTransition, extras)
        } catch (e: IllegalAccessException) {
            e.printStackTrace()
        } catch (e: InstantiationException) {
            e.printStackTrace()
        }

    }


}
