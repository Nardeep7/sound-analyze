package com.sachtech.manishpreet.soundanalyze.extensions

import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.sachtech.manishpreet.soundanalyze.basepackage.base.NetworkUtils
import com.sachtech.manishpreet.soundanalyze.basepackage.controller.App
import java.util.*


fun toast(message: String) {
    Toast.makeText(App.application.applicationContext, message, Toast.LENGTH_LONG).show()
}

fun log(message: String, title: String) {
    Log.e(title, message)
}

inline fun <reified T> Context.openActivity() {
    startActivity(Intent(App.application.applicationContext, T::class.java))
}

inline fun <reified T> Context.openActivity(key: String, value: Any) {

    var intent = Intent(App.application.applicationContext, T::class.java)
    when (value) {
        is String -> intent.putExtra(key, value as String)
        is Boolean -> intent.putExtra(key, value as Boolean)
    }
    startActivity(intent)
}


fun isPermissionsGranted(context: Context, p0: Array<String>): Boolean {
    p0.forEach {
        if (ContextCompat.checkSelfPermission(context, it) == PackageManager.PERMISSION_DENIED)
            return false
    }
    return true
}

fun askForPermissions(activity: Activity, array: Array<String>) {
    ActivityCompat.requestPermissions(activity, array, 0)
}


fun showConfirmAlert(
    message: String?, positiveText: String?
    , negativetext: String?
    , onConfirmed: () -> Unit = {}
    , onCancel: () -> Unit = { }
) {

    if (message.isNullOrEmpty()) return

    val builder = AlertDialog.Builder(App.application.applicationContext)

    builder.setMessage(message)
        .setCancelable(false)
        .setPositiveButton(positiveText) { dialog, id ->
            //do things

            onConfirmed.invoke()
            dialog.dismiss()
        }
        .setNegativeButton(negativetext) { dialog, id ->
            //do things

            onCancel.invoke()
            dialog.dismiss()
        }

    val alert = builder.create()
    alert.show()
    alert.getButton(Dialog.BUTTON_NEGATIVE).setAllCaps(false)
    alert.getButton(Dialog.BUTTON_POSITIVE).setAllCaps(false)


}


fun View.visible() {
    this.visibility = View.VISIBLE
}

fun View.gone() {
    this.visibility = View.GONE
}


fun getDateDifference(startDate: Long): String {
    var different = Calendar.getInstance().time.time - startDate

    val secondsInMilli: Long = 1000
    val minutesInMilli = secondsInMilli * 60
    val hoursInMilli = minutesInMilli * 60
    val daysInMilli = hoursInMilli * 24
    val monthInMilli = hoursInMilli * 24 * 30
    val yearInMilli = hoursInMilli * 24 * 30 * 12

    val years = different / yearInMilli
    different %= yearInMilli

    val months = different / monthInMilli
    different %= monthInMilli

    val days = different / daysInMilli
    different %= daysInMilli

    val hours = different / hoursInMilli
    different %= hoursInMilli

    val minutes = different / minutesInMilli

    if (years > 1)
        return "$years years ago"
    else if (years == 1.toLong())
        return "a year ago"
    else if (months > 1)
        return "$months months ago"
    else if (months == 1.toLong())
        return "a month ago"
    else if (days > 1)
        return "$days days ago"
    else if (days == 1.toLong())
        return "a day ago"
    else if (hours > 1)
        return "$hours hours ago"
    else if (hours == 1.toLong())
        return "a hour ago"
    else if (minutes > 1)
        return "$minutes minutes ago"
    else
        return "few seconds ago"
}


fun Context.networkAvaileble(): Boolean {
    return NetworkUtils.isNetworkAvailable(this)
}

fun hideKeyboard(activity: Activity) {
    val imm = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    //Find the currently focused view, so we can grab the correct window token from it.
    var view = activity.currentFocus
    //If no view currently has focus, create a new one, just so we can grab a window token from it
    if (view == null) {
        view = View(activity)
    }
    imm!!.hideSoftInputFromWindow(view.windowToken, 0)
}
