package com.sachtech.manishpreet.soundanalyze.basepackage.controller

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTransaction

interface Transition {

    fun performTransiton(transaction: FragmentTransaction, container: Int, baseFragment: Fragment)
}
